﻿$(document).ready(function(){
  // Toggle content
  $('[data-togglebtn="true"]').click(function(){
    $(this).parent().toggleClass('active');
  });

  // Mobile menu toggle
  $('#btnToggle').click(function(){
    $('.wrapper').toggleClass('open-mobile-menu');
  });

  // Tabs
  $('#tabs > ul > li').click(function(){
    var $this = $(this);
    $('#tabs li').removeClass('tabs__link_active');
    $('.tabs__item').removeClass('tabs__item_active');
    $this.addClass('tabs__link_active');
    var $attrLink = $this.data('link');
    $('.tabs__item[data-item = "' + $attrLink + '" ').addClass('tabs__item_active');
  });

  // Toggle list
  $('li[data-toggle="true"]').click(function(){
    $(this).find('ul').toggleClass('active');
  });

  // Toggle more
  $('[data-more="true"]').click(function(){
    $(this).parent().toggleClass('active');
  });

/*  var ctx = $("#myChart");
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["January", "Febrary", "March", "April", "May", "June"],
        datasets: [{
            label: 'Traffic by Months (estimated)',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
  });*/

});
